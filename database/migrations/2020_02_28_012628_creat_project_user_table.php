<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatProjectUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_user', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->unsignedBigInteger('user_id');
           $table->unsignedBigInteger('project_id')->nullable();
           $table->timestamps();

           $table->foreign('user_id')
           ->references('id')
           ->on('users')
           ->onDelete('restrict')
           ->onUpdate('cascade');

           $table->foreign('project_id')
           ->references('id')
           ->on('projects')
           ->onDelete('SET NULL')
           ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_user');
    }
}
