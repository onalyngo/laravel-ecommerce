@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Projects</h1>
<div class="col-lg-4 offset-lg-4">
    <h4 class="">Assigned to User: {{Auth::user()->name}}</h4>

    <hr>
    <form method="POST" action="/projects">
        @csrf
        @foreach($projects as $project)
            <input type="checkbox" name="{{$project->id}}" value="{{$project->id}}">{{$project->name}}<br>

        @endforeach
        <button class="btn btn-primary" type="submit">Add Projects</button>
    </form>
</div>

@endsection
