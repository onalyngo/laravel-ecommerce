@extends("layouts.app")
@section('content')

<h1 class="text-center py-5">Cart</h1>

@if($items != null)
<div class="container">
	<div class="row">
		<div class="col-lg-10 offset-lg-1">
			<table class="table table-striped">
				<thead>
					<tr>
                        <th></th>
						<th>Name:</th>
						<th>Price:</th>
						<th>Quantity:</th>
						<th>Subtotal:</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($items as $item)
					<tr>
                        <td>
                            <img src="{{url($item->imgPath)}}" height="100px" class="d-block">
                        </td>
						<td>{{$item->name}}</td>
						<td>{{$item->price}}</td>
						<td>{{$item->quantity}}</td>
						<td>{{$item->subtotal}}</td>
						<td>
                            <form action="/removeitem/{{$item->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Remove from Cart</button>
                            </form>
                        </td>
					</tr>
					@endforeach
					<tr>
                        <td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>Total: {{$total}}</td>
						<td>
                            <form action="/emptycart" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Empty Cart</button>
                            </form>
                        </td>
					</tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><a href="/checkout" class="btn btn-secondary">Pay via COD</a>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
				</tbody>
			</table>
		</div>

	</div>
</div>
@else

    <h2 class="text-center py-5">CART IS EMPTY, GO SHOPPING!!!!!</h2>

@endif

@endsection
